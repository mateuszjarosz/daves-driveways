<?php

include "include/header.php";

?>

<table class="table table-striped">
    <thead>
    <tr>
        <th>ID</th>
        <th>Customer</th>
        <th>Staff</th>
        <th>Date</th>
        <th>Paving Price</th>
        <th>Base Price</th>
        <th>Labour</th>
        <th>VAT</th>
        <th>Total</th>
        <th>Actions</th>
    </tr>
    </thead>
    <tbody>

    <?php
    foreach ($db->query('SELECT * FROM quotations') as $row) { ?>
        <tr>
            <td><?php echo $row['q_id']; ?></td>
            <td><?php echo $row['c_first'] . " " . $row['c_last']; ?></td>
            <td><?php echo $row['s_id']; ?></td>
            <td><?php echo $row['q_date']; ?></td>
            <td>£<?php echo $row['p_cost']; ?></td>
            <td>£<?php echo $row['b_cost']; ?></td>
            <td>£<?php echo $row['q_labour']; ?></td>
            <td>£<?php echo $row['q_vat']; ?></td>
            <td>£<?php echo $row['q_total']; ?></td>
            <td><a href="https://davesdriveways.xyz/quote.php?id=<?php echo $row['q_id']; ?>"><i class="fa fa-eye" aria-hidden="true"></i> View</a> - <a href="https://davesdriveways.xyz/delete-quote.php?id=<?php echo $row['q_id']; ?>"><i class="fa fa-trash" aria-hidden="true"></i> Delete</a></td>

        </tr>
    <?php } ?>
    </tbody>
</table>
<?php include "include/footer.php"; ?>

