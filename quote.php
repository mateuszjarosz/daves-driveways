<?php

include "include/header.php";

$row = "";

if (isset($_GET['id']) == true) {
    $sql = "SELECT *  FROM quotations WHERE q_id = '" . $_GET['id'] . "'";
    $db_result = $db->query($sql);
    $row = $db_result->fetch(PDO::FETCH_ASSOC);
} else {
    header("Location: https://davesdriveways.xyz");
}


?>

<h1 class="text-center">Quote #<?php echo $row['q_id']; ?> <br><small><?php echo $row['q_date']; ?></small></h1>
<div class="row">
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-body">
                <h2>Customer:</h2>
                <address>
                    <strong><?php echo $row['c_first'] . " " . $row['c_last']; ?></strong><br>
                    <?php echo $row['c_address']; ?><br>
                    <?php echo $row['c_town']; ?><br>
                    <?php echo $row['c_postal']; ?><br>
                    Phone:</abbr> <?php echo $row['c_phone']; ?><br>
                    Email:</abbr> <?php echo $row['c_email']; ?>
                </address>

                <?php if ($row['c_preference'] == 0){ ?>
                    <strong>Customer prefers emails.</strong>
                <?php } else { ?>
                    <strong>Customer prefers post.</strong>
                <?php } ?>

                <br><strong>Staff: </strong><?php echo $row['s_id']; ?>
            </div>
        </div>
    </div>
    <div class="col-md-6">
        <div class="panel panel-default">
            <div class="panel-body">
                Area: <?php echo $row['q_area']; ?> m2<br>
                Pavement Type: <?php echo $row['p_type']; ?><br>
                Base Type: <?php echo $row['b_type']; ?> <br><br>
                Pavement Price: £<?php echo $row['p_cost']; ?><br>
                Base Price: £<?php echo $row['b_cost']; ?><br>
                Labour: £<?php echo $row['q_labour']; ?><br>
                Subtotal: £<?php echo $row['q_total'] - $row['q_vat']; ?><br>
                VAT: £<?php echo $row['q_vat']; ?><br>
                Total: £<?php echo $row['q_total']; ?><br><br>
            </div>
        </div>
    </div>
</div>


