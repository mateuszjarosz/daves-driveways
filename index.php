<?php
if (isset($_COOKIE['s_id']) == true) {
    $staffid = $_COOKIE['s_id'];
    $loggedin = true;
} else {
    $loggedin = false;
}

if ($loggedin == true) {
    header("Location: https://davesdriveways.xyz/quotations.php");
} else {
    header("Location: https://davesdriveways.xyz/login.php");
}

?>
<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dave's Driveways</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/flatly/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-XYCjB+hFAjSbgf9yKUgbysEjaVLOXhCgATTEBpCqT1R3jvG5LGRAK5ZIyRbH5vpX" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link href="../res/style.css" rel="stylesheet">

    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<h1>Our website is under the maintenance or there has been an error.<br><small>Please contact your line manager for more information.</small></h1>
<style>
    body {
        text-align: center;
    }
</style>
</body>
</html>


<?php include "include/footer.php"; ?>


