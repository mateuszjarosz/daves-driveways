<?php

include "include/header.php";

?>

<div class="row">
    <div class="col-md-6">
        <div class="alert alert-warning" id="warning" style="visibility: hidden;">All fields are required!</div>
        <form class="form-horizontal" action="new-quotation-handler.php" method="post" id="quote" name="quote">
            <fieldset>

                <!-- Form Name -->
                <legend>New Quotation</legend>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="c_first">First Name</label>
                    <div class="col-md-6">
                        <input id="c_first" name="c_first" type="text" placeholder="" class="form-control input-md"
                               required="">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="c_last">Last Name</label>
                    <div class="col-md-6">
                        <input id="c_last" name="c_last" type="text" placeholder="" class="form-control input-md"
                               required="">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="c_email">Email</label>
                    <div class="col-md-6">
                        <input id="c_email" name="c_email" type="text" placeholder="" class="form-control input-md"
                               required="">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="c_phone">Phone</label>
                    <div class="col-md-6">
                        <input id="c_phone" name="c_phone" type="text" placeholder="" class="form-control input-md"
                               required="">

                    </div>
                </div>

                <!-- Prepended checkbox -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="c_address">Address</label>
                    <div class="col-md-6">
                        <div class="input-group">
      <span class="input-group-addon">
          <input type="checkbox" name="c_preference" id="c_preference">
      </span>
                            <input id="c_address" name="c_address" class="form-control" type="text" placeholder=""
                                   required="">
                        </div>
                        <p class="help-block">Tick the box if customer prefers postal confirmation rather than email</p>
                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="c_town">Town/City</label>
                    <div class="col-md-6">
                        <input id="c_town" name="c_town" type="text" placeholder="" class="form-control input-md"
                               required="">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="c_postal">Postal Code</label>
                    <div class="col-md-6">
                        <input id="c_postal" name="c_postal" type="text" placeholder="" class="form-control input-md"
                               required="">

                    </div>
                </div>

                <!-- Text input-->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="q_area">Area</label>
                    <div class="col-md-6">
                        <div class="input-group">
                            <input id="q_area" name="q_area" type="text" placeholder="" class="form-control input-md"
                                   required="">
                            <div class="input-group-addon">m2</div>
                        </div>

                    </div>
                </div>

                <!-- Select Basic -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="p_type">Paving Type</label>
                    <div class="col-md-6">
                        <select id="p_type" name="p_type" class="form-control">
                            <option value="1">Natural Brick</option>
                            <option value="2">Clay Brick</option>
                            <option value="3">Yorkshire Cobble</option>
                            <option value="4">Natural Granite Cobble</option>
                            <option value="5">Concrete Block</option>
                            <option value="6">Natural Slabs</option>
                            <option value="7">Clay Slabs</option>
                            <option value="8">Yorkshire Slabs</option>
                        </select>
                    </div>
                </div>

                <!-- Select Basic -->
                <div class="form-group">
                    <label class="col-md-4 control-label" for="b_type">Base Type</label>
                    <div class="col-md-6">
                        <select id="b_type" name="b_type" class="form-control">
                            <option value="1">Hardcore</option>
                            <option value="2">Concrete</option>
                        </select>
                    </div>
                </div>

                <input type="text" name="submit_p_name" id="submit_p_name" value="" style="visibility:hidden;">
                <input type="text" name="submit_p_cost" id="submit_p_cost" value="" style="visibility:hidden;">
                <input type="text" name="submit_b_name" id="submit_b_name" value="" style="visibility:hidden;">
                <input type="text" name="submit_b_cost" id="submit_b_cost" value="" style="visibility:hidden;">
                <input type="text" name="submit_labour" id="submit_labour" valu="" style="visibility:hidden;">
                <input type="text" name="submit_subtotal" id="submit_subtotal" value="" style="visibility:hidden;">
                <input type="text" name="submit_vat" id="submit_vat" value="" style="visibility:hidden;">
                <input type="text" name="submit_total" id="submit_total" value="" style="visibility:hidden;">

                <div class="form-group">
                    <div class="col-md-2"></div>
                    <div class="col-md-2">
                        <button type="button" id="check_button" class="btn btn-default" onClick="validateForm()">
                            Validate
                        </button>
                    </div>
                    <div class="col-md-4">
                        <input type="submit" id="submit_button" class="btn btn-success" value="Submit" style="visibility: hidden;">
                        
                        </input>
                    </div>
                </div>

            </fieldset>
        </form>

    </div>
    <div class="col-md-6">
        <h1>Summary</h1><br>

        <button type="button" class="button button-default" onClick="total()">Calculate</button>
        <br>
        <ul class="list-unstyled">
            <li><strong>Pavement Type:</strong>
                <span id="display_pavement_type"></span></li>
            <li><strong>Pavement Price:</strong> £
                <span id="display_pavement_price"></span></li>
            <li><strong>Base Type:</strong>
                <span id="display_base_type"></span></li>
            <li><strong>Base Price:</strong> £
                <span id="display_base_price"></span></li>
            <li><strong>Days:</strong>
                <span id="display_days"></span></li>
            <li><strong>Labour:</strong> £
                <span id="display_labour"></span></li>
            <li><strong>Subtotal:</strong> £
                <span id="display_subtotal"></span></li>
            <li><strong>VAT:</strong> £
                <span id="display_vat"></span></li>
            <li><strong>Total:</strong> £
                <span id="display_total"></span></li>
        </ul>
    </div>
</div>

<script>
    function total() {

        /* Declare all variables */

        var days = 0;

        /* Get details from Fields */

        var area = parseInt(document.getElementById('q_area').value);
        var p_type = document.getElementById('p_type').value;
        var b_type = document.getElementById('b_type').value;

        console.log("Area: " + area);

        /* Pavement Calculations */

        switch (p_type) {
            case "1":
                p_name = "Natural Brick";
                p_area = 0.02;
                p_cost = 0.50;
                break;
            case "2":
                p_name = "Clay Brick";
                p_area = 0.02;
                p_cost = 0.75;
                break;
            case "3":
                p_name = "Yorkshire Cobble";
                p_area = 0.08;
                p_cost = 0.75;
                break;
            case "4":
                p_name = "Natural Granite Cobble";
                p_area = 0.08;
                p_cost = 0.75;
                break;
            case "5":
                p_name = "Concrete Block";
                p_area = 0.01;
                p_cost = 0.40;
                break;
            case "6":
                p_name = "Natural Slabs";
                p_area = 0.36;
                p_cost = 5.50;
                break;
            case "7":
                p_name = "Clay Slabs";
                p_area = 0.36;
                p_cost = 7.50;
                break;
            case "8":
                p_name = "Yorkshire Slabs";
                p_area = 0.36;
                p_cost = 7.50;
                break;
        }

        p_blocks = (area / p_area) * 1.05;
        pTotalCost = p_blocks * p_cost;

        console.log("pTotalCost: " + pTotalCost.toFixed(2));

        /* Base Calculation */

        switch (b_type) {
            case "1":
                b_name = "Hardcore";
                b_price = 2;
                break;
            case "2":
                b_name = "Concrete";
                b_price = 4;
                break;
        }

        bTotalCost = area * b_price;

        console.log("bTotalCost: " + bTotalCost.toFixed(2));

        /* Calculate days */

        if (area <= 50) {
            days = 2;
        } else {
            days = Math.ceil((area - 50) / 40) + 2;
        }

        console.log("Days: " + days);

        /* Calculate Labour */

        labour = days * 250;

        console.log("Labour: " + labour.toFixed(2));

        /* Calculate Totals */

        subtotal = pTotalCost + bTotalCost + labour;

        console.log("Subtotal: " + subtotal.toFixed(2));

        total = subtotal * 1.20;
        vat = total - subtotal;

        console.log("VAT: " + vat.toFixed(2));
        console.log("Total: " + total.toFixed(2));

        document.getElementById("submit_p_name").value = String(p_name);
        document.getElementById("submit_p_cost").value = String(pTotalCost.toFixed(2));
        document.getElementById("submit_b_name").value = String(b_name);
        document.getElementById("submit_b_cost").value = String(bTotalCost.toFixed(2));
        document.getElementById("submit_labour").value = String(labour.toFixed(2));
        document.getElementById("submit_subtotal").value = String(subtotal.toFixed(2));
        document.getElementById("submit_vat").value = String(vat.toFixed(2));
        document.getElementById("submit_total").value = String(total.toFixed(2));

        document.getElementById("display_pavement_type").innerHTML = p_name;
        document.getElementById("display_pavement_price").innerHTML = pTotalCost.toFixed(2);
        document.getElementById("display_base_type").innerHTML = b_name;
        document.getElementById("display_base_price").innerHTML = bTotalCost.toFixed(2);
        document.getElementById("display_days").innerHTML = days.toString();
        document.getElementById("display_labour").innerHTML = labour.toFixed(2);
        document.getElementById("display_subtotal").innerHTML = subtotal.toFixed(2);
        document.getElementById("display_vat").innerHTML = vat.toFixed(2);
        document.getElementById("display_total").innerHTML = total.toFixed(2);




    }

    function validateForm() {
        var fields = ["c_first", "c_last", "c_email", "c_phone", "c_address", "c_town", "c_postal", "q_area"];

        var i, l = fields.length;
        var fieldname;
        for (i = 0; i < l; i++) {
            fieldname = fields[i];
            if (document.forms["quote"][fieldname].value === "") {
                document.getElementById('submit_button').style.visibility = 'hidden';
                document.getElementById('warning').style.visibility = 'visible';
            } else {
                document.getElementById('submit_button').style.visibility = 'visible';
                document.getElementById('warning').style.visibility = 'hidden';
            }
        }
        return true;
    }
</script>

<?php include "include/footer.php" ?>
