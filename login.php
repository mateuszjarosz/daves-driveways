<?php
if (isset($_COOKIE['s_id']) == true) {
    $staffid = $_COOKIE['s_id'];
    $loggedin = true;
} else {
    $loggedin = false;
}

if ($loggedin == true) {
    header("Location: https://davesdriveways.xyz/quotations.php");
}

?>

<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Dave's Driveways</title>

    <link href="https://maxcdn.bootstrapcdn.com/bootswatch/3.3.6/flatly/bootstrap.min.css" rel="stylesheet"
          integrity="sha384-XYCjB+hFAjSbgf9yKUgbysEjaVLOXhCgATTEBpCqT1R3jvG5LGRAK5ZIyRbH5vpX" crossorigin="anonymous">
    <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css" rel="stylesheet"
          integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link href="../res/style.css" rel="stylesheet">

    <meta name="viewport" content="width=device-width, initial-scale=1">
</head>
<body>
<div class="container-fluid">
    <div class="row">
        <div class="col-md-3"></div>
        <div class="col-md-6">
            <div class="panel panel-default">
                <div class="panel-body">
                    <?php if(isset($_GET['error']) == true) { ?>
                    <div class="alert alert-danger">Incorrect id or passcode. Please try again or contact your line manager</div>
                    <?php } ?>
                    <form action="loginhandler.php" method="post" class="form-horizontal">
                        <fieldset>
                            <legend>Login</legend>
                            <!-- Text input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="s_id">Staff ID</label>
                                <div class="col-md-6">
                                    <input id="s_id" name="s_id" type="number" placeholder=""
                                           class="form-control input-md"
                                           required="">

                                </div>
                            </div>

                            <!-- Password input-->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="s_passcode">Staff Passcode</label>
                                <div class="col-md-6">
                                    <input id="s_passcode" name="s_passcode" type="password" pattern="[0-9]*"
                                           inputmode="numeric" placeholder=""
                                           class="form-control input-md" required="">

                                </div>
                            </div>

                            <!-- Button -->
                            <div class="form-group">
                                <label class="col-md-4 control-label" for="submit"></label>
                                <div class="col-md-4">
                                    <button id="submit" name="submit" class="btn btn-success">Login</button>
                                </div>
                            </div>

                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
        <div class="col-md-3"></div>
    </div>

    <style>
        body {
            background-color: #2C3E50;
        }
    </style>


    <?php include "include/footer.php"; ?>
