<?php

include "include/header.php";

?>

<div class="row">
    <div class="col-md-4">
        <img class="img-responsive" src="https://farm1.staticflickr.com/45/127410037_087b6197c8_o_d.jpg">
        <p>Natural Brick</p>
    </div>
    <div class="col-md-4">
        <img class="img-responsive" src="https://farm4.staticflickr.com/3834/13726436584_21bef4521b_o_d.jpg">
        <p>Clay Brick</p>
    </div>
    <div class="col-md-4">
        <img class="img-responsive" src="/res/p3.png">
        <p>Yorkshire Cobble</p>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <img class="img-responsive" src="https://farm1.staticflickr.com/180/391949955_0b6818797c_o_d.jpg">
        <p>Natural Granite Cobble</p>
    </div>
    <div class="col-md-4">
        <img class="img-responsive" src="https://farm4.staticflickr.com/3052/2369452288_16165a5f8b_o_d.jpg">
        <p>Concrete Block</p>
    </div>
    <div class="col-md-4">
        <img class="img-responsive" src="https://farm1.staticflickr.com/123/317977540_7532970a3d_o_d.jpg">
        <p>Natural Slabs</p>
    </div>
</div>
<div class="row">
    <div class="col-md-4">
        <img class="img-responsive" src="https://farm7.staticflickr.com/6133/6041403936_0d9def63bd_o_d.jpg">
        <p>Clay Slabs</p>
    </div>
    <div class="col-md-4">
        <img class="img-responsive" src="https://farm7.staticflickr.com/6148/6041448910_3474b9d241_o_d.jpg">
        <p>Yorkshire Slabs</p>
    </div>
</div>